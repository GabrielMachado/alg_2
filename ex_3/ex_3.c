#include "Index_FIle_List.h"

static char* read_current_input(FILE *file_pointer, char *line) {
    char current_char = fgetc(file_pointer);
    int i = 0;
    while (current_char != EOF && current_char != RECORD_DELIMITER_CHAR) {
        line[i] = current_char;
        ++i;
        current_char = fgetc(file_pointer);
    }

    if (current_char == RECORD_DELIMITER_CHAR) {
        line[i] = '@';
        ++i;
    }

    line[i] = '\0';

    return line;
}

int search_record_option() {
    int key_to_found, offset;
    scanf("%d", &key_to_found);
    char current_string[120];
    offset = search_record(key_to_found);
    if (offset == -1) {
        printf("Registro nao encontrado!\n");
    } else {
        FILE *file_data_pointer;
        file_data_pointer = fopen(DATA_FILE_NAME, "r");
        fseek(file_data_pointer, offset, SEEK_CUR);
        read_current_input(file_data_pointer, current_string);
        record_t *record = malloc(RECORD_SIZE);
        csv_to_record(record, current_string);
        print_record(record);
        fclose(file_data_pointer);
    }

    return 0;
}

int delete_record_option() {
    int key_to_found, offset;
    scanf("%d", &key_to_found);

    offset = search_record(key_to_found);

    if (offset != -1) {
        int record_amount;

        FILE *file_index_pointer;
        file_index_pointer = fopen(INDEX_FILE_NAME, "r");

        fseek(file_index_pointer, 0, SEEK_END);
        record_amount = ftell(file_index_pointer) / INDEX_SIZE;

        index_file_array_t* root = malloc(sizeof(index_file_array_t));
        root->content = NULL;
        root->prox = NULL;

        index_file_array_t* first_item = malloc(sizeof(index_file_array_t));
        first_item->content = NULL;
        first_item->prox = NULL;
        root->prox = first_item;

        fill_index_array(0, record_amount, first_item, file_index_pointer);
        fclose(file_index_pointer);

        remove_list(root, key_to_found);
        rewrite_index_list(root);
        free_array(root);
    }

    return 0;
}

int insert_record_option() {
    char input[255];
    fgets(input, 255, stdin);
    fflush(stdin);

    record_t *record = malloc(RECORD_SIZE);
    csv_to_record(record, input);

    if (search_record(record->num_USP) == -1) {
        write_record(record);
    } else {
        printf("O Registro ja existe!\n");
    }

    free(record);

    return 0;
}

int read_input() {
    char option[6];
    scanf("%s", option);

    if (strcmp(option, "exit") == 0) return 1;
    if (strcmp(option, "insert") == 0) return insert_record_option();
    if (strcmp(option, "search") == 0) return search_record_option();
    if (strcmp(option, "delete") == 0) return delete_record_option();
    return 0;
}

int main() {
    int exit = 0;

    do {
        exit = read_input();
    } while (exit != 1);

    return 0;
}
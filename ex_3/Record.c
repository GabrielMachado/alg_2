#include "Record.h"
# include "Index_File.h"

int RECORD_SIZE = sizeof(struct record);

void print_record (record_t *record) {
    printf("-------------------------------\n");
	printf("USP number: %d\n", record->num_USP);
	printf("Name: %s\n", record->first_name);
    printf("Surname: %s\n", record->last_name);
	printf("Course: %s\n", record->course);
	printf("Test grade: %0.2f\n", record->score);
    printf("-------------------------------\n");
}

// /**
//  * Formatt a csv line to a record object
//  * Example: `1,My Name, Jonas,Sistemas de Informacao,10.00`
//  */
void csv_to_record(record_t *record, char line[255]) {
    char *token = strtok(line, ",");
    record->num_USP = atoi(token);

    token = strtok(NULL, ",");
    strcpy(record->first_name, token);

    token = strtok(NULL, ",");
    strcpy(record->last_name, token);

    token = strtok(NULL, ",");
    strcpy(record->course, token);

    token = strtok(NULL, ",");
    record->score = atoi(token);
}

void write_record(record_t *record) {
    FILE *file_data_pointer;
    file_data_pointer = fopen(DATA_FILE_NAME, "a+");

    write_index(record);

    fprintf(file_data_pointer, "%d", record->num_USP);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%s", record->first_name);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%s", record->last_name);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%s", record->course);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%0.2f", record->score);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%c", RECORD_DELIMITER_CHAR);

    fclose(file_data_pointer);
}
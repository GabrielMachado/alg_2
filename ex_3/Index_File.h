#ifndef INDEX_FILE_H_

#include "Record.h"

#define INDEX_FILE_NAME "index_file.dat"

int INDEX_SIZE;

typedef struct index_file {
  int primary_key;
  int offset;
} index_file_t;

void print_index(index_file_t *index);
void write_index(record_t *record);
int search_record(int key_to_found); 

#endif // INDEX_FILE_H_
#include "Index_File.h"

#ifndef INDEX_FILE_LIST_H_

typedef struct index_file_array {
    index_file_t* content;
    struct index_file_array *prox;
} index_file_array_t;

void rewrite_index_list(index_file_array_t* root);
void remove_list(index_file_array_t* root, int key);
int find_index_file(index_file_array_t* root, int key);
void free_array(index_file_array_t* root);
void fill_index_array(int i, int max, index_file_array_t* root, FILE *file_index_pointer);
int search_record(int key_to_found);

#endif // INDEX_FILE_H_
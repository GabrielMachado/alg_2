#include "Index_File.h"

int INDEX_SIZE = sizeof(struct index_file);

void print_index(index_file_t *index) {
    printf("-------------------------------\n");
    printf("%d\n", index->primary_key);
    printf("%d\n", index->offset);
    printf("-------------------------------\n");
}

void write_index(record_t *record) {
    FILE *file_index_pointer;
    file_index_pointer = fopen(INDEX_FILE_NAME, "a+b");

    FILE *file_data_pointer;
    file_data_pointer = fopen(DATA_FILE_NAME, "r");

    fseek(file_data_pointer, 0, SEEK_END);
    index_file_t *index = malloc(INDEX_SIZE);
    index->primary_key = record->num_USP;
    index->offset = ftell(file_data_pointer);

    fwrite(index, INDEX_SIZE, 1, file_index_pointer);

    free(index);
    fclose(file_index_pointer);
    fclose(file_data_pointer);
}
#ifndef RECORD_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RECORD_H_

#define DATA_FILE_NAME "reg.dat"
#define FIELD_DELIMITER_CHAR ','
#define RECORD_DELIMITER_CHAR '@'

int RECORD_SIZE;

typedef struct record { 
  int num_USP;
  char first_name[25];
  char last_name[25];
  char course[50];
  float score;
} record_t;

void print_record (record_t *record);
void write_record(record_t *record);
void csv_to_record(record_t *record, char line[255]);

#endif // RECORD_H_
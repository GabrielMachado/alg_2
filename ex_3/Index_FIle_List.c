#include "Index_FIle_List.h"

/**
 * Create a new data file based on a linked list
 */
void rewrite_index_list(index_file_array_t* root) {
    FILE *new_file_index_pointer;
    new_file_index_pointer = fopen("temp.dat", "a+b");

    while (root != NULL) {
        if (root && root->content) fwrite(root->content, INDEX_SIZE, 1, new_file_index_pointer);
        root = root->prox;
    }
    remove(INDEX_FILE_NAME);
    rename("temp.dat", INDEX_FILE_NAME);
    fclose(new_file_index_pointer);
}

void remove_list(index_file_array_t* root, int key) {
    if (root && root->prox) {
        index_file_array_t *prox = root->prox;
        if (prox->content->primary_key == key) {
            root->prox = prox->prox;
            free(prox);
        } else {
            remove_list(prox, key);
        }
    }
}

int find_index_file(index_file_array_t* root, int key) {
    if (root && root->content && root->content->primary_key == key) return root->content->offset;
    if (root != NULL && root->prox != NULL) return find_index_file(root->prox, key);
    return -1;
}

void free_array(index_file_array_t* root) {
    if (root->prox != NULL) free_array(root->prox);
    free(root);
}

void fill_index_array(int i, int max, index_file_array_t* root, FILE *file_index_pointer) {
    if (i < max) {
        index_file_t *index = malloc(INDEX_SIZE);
        fseek(file_index_pointer, i * INDEX_SIZE, SEEK_SET);
	    fread(index, INDEX_SIZE, 1, file_index_pointer);
        root->content = index;
    
        root->prox = malloc(sizeof(index_file_array_t));
        root->prox->content = NULL;
        root->prox->prox = NULL;

        fill_index_array(i + 1, max, root->prox, file_index_pointer);
    }
}

int search_record(int key_to_found) {
    FILE *file_index_pointer;
    file_index_pointer = fopen(INDEX_FILE_NAME, "r");

    int offset = -1;

    if(file_index_pointer != NULL) {
        int record_amount;

        fseek(file_index_pointer, 0, SEEK_END);
        record_amount = ftell(file_index_pointer) / INDEX_SIZE;

        index_file_array_t* root = malloc(sizeof(index_file_array_t));
        root->content = NULL;
        root->prox = NULL;

        fill_index_array(0, record_amount, root, file_index_pointer);
        offset = find_index_file(root, key_to_found);

        free_array(root);
        fclose(file_index_pointer);
     }

    return offset;
}

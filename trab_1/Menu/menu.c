#include "menu.h"
#include "../Btree/b_tree.h"
#include "../Record/record_file.h"

int read_input() {
    char option[6];
    scanf("%s", option);

    if (strcmp(option, "exit") == 0) return 1;
    if (strcmp(option, "insert") == 0) return insert_record_option();
    if (strcmp(option, "search") == 0) return search_record_option();
    if (strcmp(option, "update") == 0) return update_record_option();
    return 0;
}

int insert_record_option() {
    char input[255];
    fgets(input, 255, stdin);
    fflush(stdin);

    record_t *record = malloc(RECORD_SIZE);
    csv_to_record(record, input);

    int current_record_offset = tree_search(record->num_USP);

    if (current_record_offset == -1) { // if element does not exist yet we need to write record in file and insert it into B Tree
        int offset = write_record(record);
        tree_item_t *element;
        element = malloc(sizeof(tree_item_t));
        element->key = record->num_USP;
        element->record_RRN = offset;

        tree_insert(element);
    } else {
        printf("O Registro ja existe!\n");
    }

    free(record);
    return 0;
}

int search_record_option() {
    int p_key;
    int RRN;

    scanf("%d", &p_key);
    RRN = tree_search(p_key);

    if (RRN == -1) printf("Registro nao encontrado!\n");
    else {
        record_t *record;
        record = malloc(RECORD_SIZE);
        get_record(record, RRN);
        print_record(record);
        free(record);
    }

    return 0;
}

int update_record_option() {
    char input[255];
    fgets(input, 255, stdin);
    fflush(stdin);

    record_t *record = malloc(RECORD_SIZE);
    csv_to_record(record, input);

    int current_record_offset = tree_search(record->num_USP);
    if (current_record_offset != -1) {
        // TODO mark old record to be deleted
        int offset = write_record(record);
        tree_item_t *element;
        element = malloc(sizeof(tree_item_t));
        element->key = record->num_USP;
        element->record_RRN = offset;

        tree_update(element);
    } else {
        printf("Registro nao encontrado!\n");
    }

    free(record);
    return 0;
}
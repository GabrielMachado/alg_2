#ifndef MENU_H_
#define MENU_H_

#include "../Record/record.h"
#include "../Btree/b_tree.h"

int read_input(); // read user action and call proper controller
int insert_record_option(); // insert controller
int search_record_option(); // search controller
int update_record_option(); // update controller

#endif // MENU_H_
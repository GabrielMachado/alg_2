#include "./Menu/menu.h"
#include "./Btree/b_tree.h"

int main() {
    int exit = 0;

    do {
        exit = read_input();
    } while (exit != 1);

    return 0;
}
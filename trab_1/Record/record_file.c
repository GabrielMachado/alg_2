#include "record_file.h"
#include "../Menu/menu.h"

int write_record(record_t *record) {
    FILE *file_data_pointer;
    file_data_pointer = fopen(DATA_FILE_NAME, "a+");

    fseek(file_data_pointer, 0, SEEK_END);
    int RRN = ftell(file_data_pointer);

    fprintf(file_data_pointer, "%d", record->num_USP);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%s", record->first_name);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%s", record->last_name);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%s", record->course);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%0.2f", record->score);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    fprintf(file_data_pointer, "%c", RECORD_DELIMITER_CHAR);

    fclose(file_data_pointer);

    return RRN;
}

void get_record(record_t *record, int RRN) {
    char current_string[120];
    FILE *file_data_pointer;
    file_data_pointer = fopen(DATA_FILE_NAME, "r");

    fseek(file_data_pointer, RRN, SEEK_CUR);

    read_current_input(file_data_pointer, current_string);
    csv_to_record(record, current_string);

    fclose(file_data_pointer);
}

char* read_current_input(FILE *file_pointer, char *line) {
    char current_char = fgetc(file_pointer);
    int i = 0;
    while (current_char != EOF && current_char != RECORD_DELIMITER_CHAR) {
        line[i] = current_char;
        ++i;
        current_char = fgetc(file_pointer);
    }

    if (current_char == RECORD_DELIMITER_CHAR) {
        line[i] = '@';
        ++i;
    }

    line[i] = '\0';

    return line;
}

void csv_to_record(record_t *record, char line[255]) {
    char *token = strtok(line, ",");
    record->num_USP = atoi(token);

    token = strtok(NULL, ",");
    remove_quotation(token);
    strcpy(record->first_name, token);

    token = strtok(NULL, ",");
    remove_quotation(token);
    strcpy(record->last_name, token);

    token = strtok(NULL, ",");
    remove_quotation(token);
    strcpy(record->course, token);

    token = strtok(NULL, ",");
    record->score = atof(token);
}

void remove_quotation(char* s){
    int j, n = strlen(s);
    for (int i = j = 0; i < n; i++) {
        if (s[i] != '\"') s[j++] = s[i];
    }
 
    s[j] = '\0';
}
#include "record.h"

int RECORD_SIZE = sizeof(struct record);

void print_record (record_t *record) {
    printf("-------------------------------\n");
	printf("nUSP: %d\n", record->num_USP);
	printf("Nome: %s\n", record->first_name);
    printf("Sobrenome: %s\n", record->last_name);
	printf("Curso: %s\n", record->course);
	printf("Nota: %0.2f\n", record->score);
    printf("-------------------------------\n");
}

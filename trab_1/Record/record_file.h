#ifndef RECORD_FILE_H_
#define RECORD_FILE_H_

#include "record.h"

#define DATA_FILE_NAME "reg.dat"
#define FIELD_DELIMITER_CHAR ','
#define RECORD_DELIMITER_CHAR '@'

char* read_current_input(FILE *file_pointer, char *line); // return a csv line that was saved on DATA_FILE_NAME
void csv_to_record(record_t *record, char line[255]); // parse a CSV line to a record
void remove_quotation(char *str); // parse string to remove quotation marks

int write_record(record_t *record); // return the offset inserted
void get_record(record_t *record, int RRN); // fill a record that starts on the given offset

#endif // RECORD_FILE_H_
#ifndef RECORD_H_
#define RECORD_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int RECORD_SIZE;

typedef struct record { 
    int num_USP;
    char first_name[25];
    char last_name[25];
    char course[50];
    float score;
} record_t;

void print_record(record_t *record);

#endif // RECORD_H_
#ifndef B_TREE_FILE_H_
#define B_TREE_FILE_H_

#include <stdio.h>
#include <stdlib.h>
#include "b_tree.h"
#include "../Record/record_file.h"

#define HEADER_FILE_NAME "tree_header.dat"
#define B_TREE_FILE_NAME "tree_data.dat"

int get_or_create_root();
bt_page* get_page_by_rrn(int rrn);
void update_page(bt_page *node, int RRN);
int write_page (bt_page *page);
void write_root (int rrn);
#endif // B_TREE_FILE_H_

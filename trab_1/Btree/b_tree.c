#include "./b_tree.h"
#include "./b_tree_file.h"

bt_page* creat_empty_page() {
    bt_page *root;
    root = malloc(sizeof(bt_page));
    root->child_count = 0;
    root->childs = malloc(sizeof(int) * BTREE_ORDEM + 1);
    for(int i = 0; i <= BTREE_ORDEM; ++i) {
        root->childs[i] = -1;
    }
    root->records = malloc(sizeof(root->records) * BTREE_ORDEM);
    for(int i = 0; i < BTREE_ORDEM; ++i) {
        root->records[i].key = -1;
        root->records[i].record_RRN = -1;
    }
    root->records_count = 0;
    root->is_leaf = 1;

    return root;
}

int search(int page_rrn, int key) {
    if (page_rrn != -1) {
        bt_page *page = get_page_by_rrn(page_rrn);
        int i = 0;
        while (i < page->records_count) {
            if (key == page->records[i].key) return page->records[i].record_RRN;
            if (key < page->records[i].key && i < page->child_count) {
                return search(page->childs[i], key);
            }
            ++i;
        }
        if (i < page->child_count) {
            return search(page->childs[i], key);
        }
    }
    return -1;
}

int tree_search(int key) {
    int rootRRN = get_or_create_root();
    int record_RRN = search(rootRRN, key);
    return record_RRN;
}

void update(int page_rrn, tree_item_t *item) {
    if (page_rrn != -1) {
        bt_page *page = get_page_by_rrn(page_rrn);
        int i = 0;
        while (i < page->records_count) {
            if (item->key == page->records[i].key) {
                page->records[i].record_RRN = item->record_RRN;
                update_page(page, page_rrn);
            }
            if (item->key < page->records[i].key && i < page->child_count) {
                update(page->childs[i], item);
            }
            ++i;
        }
        if (i < page->child_count) {
            update(page->childs[i], item);
        }
    }
}

void print_page (bt_page *page) {
    printf("records_count: %d\n", page->records_count);
    for (int i = 0; i < page->records_count; ++i) {
        printf("record %d has key %d\n", i, page->records[i].key);
    }

    printf("child_count: %d\n", page->child_count);
    for (int i = 0; i < page->child_count; ++i) {
        printf("child %d has rrn %d\n", i, page->childs[i]);
    }
    
    printf("is_leaf: %d\n", page->is_leaf);
}

void print_node_elements(bt_page *page) {
    printf("| ");
    for(int i = 0; i < page->records_count; ++i) {
        printf("%d | ", page->records[i].key);
    }
    printf("\n");
}

void print_everything(bt_page *page) {
    print_node_elements(page);
    for (int i = 0; i < page->child_count; ++i) {
        printf("leaf %d: ", i);
        bt_page *page_aux = get_page_by_rrn(page->childs[i]);
        print_everything(page_aux);
        free(page_aux);
    }
}

void insert_sorted_array(bt_page *node, tree_item_t *element) {
    if (node->records_count == 0) {
        node->records[node->records_count] = *element;
    } else {
        int i = node->records_count - 1;
        while(element->key < node->records[i].key && i>=0) {
            node->records[i+1] = node->records[i];
            i--;
        }
        node->records[i+1] = *element;
    }
    node->records_count += 1;
}

int find_leaf_index (bt_page *page, tree_item_t *item) {
    int i = 0;
    int index = -1;
    while (i < page->records_count && index == -1) {
        if (page->records[i].key > item->key) {
            index = i;
        }
        ++i;
    }
    if (index == -1) {
        index = page->records_count; // insert on the last position
    }

    return index;
}

promoted_key_t* insert(int page_rrn, tree_item_t *item, int force_insert) {
    bt_page *page = get_page_by_rrn(page_rrn);
    // printf("insert here %d:\n", page->is_leaf);
    // print_node_elements(page);
    if (page->is_leaf || force_insert) {
        insert_sorted_array(page, item);
        if (page->records_count < BTREE_ORDEM) {
            update_page(page, page_rrn);
            free(page);

            return NULL;
        } else {
            bt_page *first_leaf, *second_leaf;
            first_leaf = creat_empty_page();
            second_leaf = creat_empty_page();

            for (int i = 0; i < (BTREE_ORDEM / 2); ++i) {
                insert_sorted_array(first_leaf, &page->records[i]);
            }

            for (int i = (BTREE_ORDEM / 2) + 1; i < page->records_count; ++i) {
                insert_sorted_array(second_leaf, &page->records[i]);
            }

            promoted_key_t *promoted;
            promoted = malloc(sizeof(promoted_key_t));
            promoted->key = page->records[(BTREE_ORDEM / 2)].key;
            promoted->record_RRN = page->records[(BTREE_ORDEM / 2)].record_RRN;

            if (page->child_count > 0) {
                for (int i = 0; i < (page->child_count / 2); ++i) {
                    bt_page *temp_page = get_page_by_rrn(page->childs[i]);
                    if (temp_page->records[0].key < promoted->key) {
                        first_leaf->childs[first_leaf->child_count] = page->childs[i];
                        first_leaf->child_count += 1;
                        first_leaf->is_leaf = 0;
                    } else {
                        second_leaf->childs[second_leaf->child_count] = page->childs[i];
                        second_leaf->child_count += 1;
                        second_leaf->is_leaf = 0;
                    }
                    free(temp_page);
                }

                for (int i = (page->child_count / 2); i < page->child_count; ++i) {
                    bt_page *temp_page = get_page_by_rrn(page->childs[i]);
                    if (temp_page->records[0].key < promoted->key) {
                        first_leaf->childs[first_leaf->child_count] = page->childs[i];
                        first_leaf->child_count += 1;
                        first_leaf->is_leaf = 0;
                    } else {
                        second_leaf->childs[second_leaf->child_count] = page->childs[i];
                        second_leaf->child_count += 1;
                        second_leaf->is_leaf = 0;
                    }
                    free(temp_page);
                }
            }

            update_page(first_leaf, page_rrn);
            promoted->childs[0] = page_rrn;
            promoted->childs[1] = write_page(second_leaf);

            free(first_leaf);
            free(second_leaf);
            free(page);

            return promoted;
        }
    } else {
        int leaf_to_insert = find_leaf_index(page, item);
        promoted_key_t *promoted = insert(page->childs[leaf_to_insert], item, 0);
        if (promoted != NULL) {
            tree_item_t *promoted_as_item;
            promoted_as_item = malloc(sizeof(tree_item_t));
            promoted_as_item->key = promoted->key;
            promoted_as_item->record_RRN = promoted->record_RRN;

            // mergeia os childrens que vieram da página que sofreu overflow com os atuais folhas
            int i = page->child_count - 1;
            while (i >= leaf_to_insert) { // bota todos para a direita até chegar no index do overflowed
                page->childs[i+1] = page->childs[i];
                --i;
            }
            page->childs[leaf_to_insert] = promoted->childs[0];
            page->childs[leaf_to_insert + 1] = promoted->childs[1];
            page->child_count += 1;

            update_page(page, page_rrn);
            promoted_key_t *new_promoted = insert(page_rrn, promoted_as_item, 1);

            free(promoted_as_item);
            free(promoted);

            return new_promoted;
        }
    }

    free(page);
    return NULL;
}

void tree_insert(tree_item_t *item) {
    int rootRRN = get_or_create_root();
    promoted_key_t *promoted = insert(rootRRN, item, 0);

    if (promoted != NULL) {
        bt_page *new_root = creat_empty_page();
        tree_item_t *new_item = malloc(sizeof(tree_item_t));
        new_item->key = promoted->key;
        new_item->record_RRN = promoted->record_RRN;
        insert_sorted_array(new_root, new_item);
        new_root->childs[0] = promoted->childs[0];
        new_root->childs[1] = promoted->childs[1];
        new_root->child_count = 2;
        new_root->is_leaf = 0;

        int rrn_new_root = write_page(new_root);
        write_root(rrn_new_root);

        free(promoted);
        free(new_root);
        free(new_item);
    }

    // for debug only
    // rootRRN = get_or_create_root(rootRRN);
    // bt_page *root = get_page_by_rrn(rootRRN);
    // print_everything(root);
    // free(root);
}

void tree_update(tree_item_t *item) {
    int rootRRN = get_or_create_root();
    update(rootRRN, item);
}

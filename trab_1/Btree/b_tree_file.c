#include "b_tree_file.h"

void write_root (int rrn) {
    FILE *new_file_index_pointer;
    new_file_index_pointer = fopen("temp.dat", "a+");

    fprintf(new_file_index_pointer, "%d", rrn);
    fprintf(new_file_index_pointer, "%c", RECORD_DELIMITER_CHAR);

    remove(HEADER_FILE_NAME);
    rename("temp.dat", HEADER_FILE_NAME);
    fclose(new_file_index_pointer);
}

void print_page_in_file(bt_page *page, FILE *file_data_pointer) {
    long int initial_pos = ftell(file_data_pointer);

    fprintf(file_data_pointer, "%d", page->records_count);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);
    
    for(int i = 0; i < BTREE_ORDEM; ++i) {
        fprintf(file_data_pointer, "%d", page->records[i].key);
        fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

        fprintf(file_data_pointer, "%d", page->records[i].record_RRN);
        fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);
    }

    fprintf(file_data_pointer, "%d", page->child_count);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);
    for(int i = 0; i <= BTREE_ORDEM; ++i) {
        fprintf(file_data_pointer, "%d", page->childs[i]);
        fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);
    }

    fprintf(file_data_pointer, "%d", page->is_leaf);
    fprintf(file_data_pointer, "%c", FIELD_DELIMITER_CHAR);

    // alocate and delimit free space
    long int final_position =  ftell(file_data_pointer);
    long int free_space = PAGESIZE - (final_position - initial_pos);
    for(int i = 0; i < free_space; ++i) {
        fprintf(file_data_pointer, "%c", '@');
    }
}

void update_page (bt_page *page, int rrn) {
    FILE *file_data_pointer;
    file_data_pointer = fopen(B_TREE_FILE_NAME, "r+");
    fseek(file_data_pointer, rrn, SEEK_SET);

    print_page_in_file(page, file_data_pointer);

    fclose(file_data_pointer);
}

int write_page (bt_page *page) {
    FILE *file_data_pointer;
    file_data_pointer = fopen(B_TREE_FILE_NAME, "a+");

    fseek(file_data_pointer, 0, SEEK_END);
    int RRN = ftell(file_data_pointer);

    print_page_in_file(page, file_data_pointer);

    fclose(file_data_pointer);
    return RRN;
}

void csv_to_page (bt_page *page, char line[255]) {
    char *token = strtok(line, ",");
    page->records_count = atoi(token);


    for(int i = 0; i < BTREE_ORDEM; ++i) {
        token = strtok(NULL, ",");
        page->records[i].key = atoi(token);

        token = strtok(NULL, ",");
        page->records[i].record_RRN = atoi(token);
    }

    token = strtok(NULL, ",");
    page->child_count = atoi(token);

    for(int i = 0; i <= BTREE_ORDEM; ++i) {
        token = strtok(NULL, ",");
        page->childs[i] = atoi(token);
    }

    token = strtok(NULL, ",");
    page->is_leaf = atoi(token);
}

bt_page* get_page_by_rrn (int rrn) {
    FILE *file_data_pointer;
    file_data_pointer = fopen(B_TREE_FILE_NAME, "r");
    char current_string[255];

    fseek(file_data_pointer, rrn, SEEK_SET);
    read_current_input(file_data_pointer, current_string);
    bt_page *page = creat_empty_page();
    csv_to_page(page, current_string);

    fclose(file_data_pointer);
    return page;
}

int get_or_create_root() {
    FILE *file_header_pointer;
    file_header_pointer = fopen(HEADER_FILE_NAME, "a+");

    char current_string[120];
    read_current_input(file_header_pointer, current_string);

    char *token = strtok(current_string, "@");
    if (token) { // already has and rrn stored
        int rootRRN;
        rootRRN = atoi(token);

        fclose(file_header_pointer);
        return rootRRN;
    } else {
        fclose(file_header_pointer);
        bt_page *root = creat_empty_page();
        int rootRRN = write_page(root);
        write_root(rootRRN);
        return rootRRN;
    }
}

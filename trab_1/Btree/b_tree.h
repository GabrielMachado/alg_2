#ifndef BTREE_H_
#define BTREE_H_

#define BTREE_ORDEM 204
#define PAGESIZE 4096

#include <stdio.h>
#include <stdlib.h>

typedef struct tree_item {
    int key;
    int record_RRN;
} tree_item_t;

typedef struct page {
    struct tree_item *records;
    int *childs;

    int child_count;
    int records_count;

    int is_leaf;
} bt_page;

typedef struct promoted_key {
    int record_RRN;
    int key;
    int childs[2];
} promoted_key_t;

int tree_search(int key);
void tree_insert(tree_item_t *item);
void tree_update(tree_item_t *item);
bt_page* creat_empty_page();
#endif // BTREE_H_
#include "b_tree.h"

b_tree_t* create_node() {
    b_tree_t* node;
    node = malloc(sizeof(b_tree_t));

    node->records = malloc(BTREE_ORDEM * sizeof *node->records);
    node->records_count = 0;

    node->childs = malloc(BTREE_ORDEM * sizeof *node->childs);
    node->child_count = 0;

    node->node_parent = NULL;
    node->is_leaf = 1;
    node->is_root = 0;

    return node;
}

int search_tree(b_tree_t *node, int key) {
    if (node != NULL) {
        int i = 0;
        while (i < node->records_count) {
            if (key == node->records[i].key) return node->records[i].RRN;
            if (key < node->records[i].key && i < node->child_count) {
                return search_tree(&node->childs[i], key);
            }
            ++i;
        }
        if (i < node->child_count) {
            return search_tree(&node->childs[i], key);
        }
    }
    return -1;
}

void insert_sorted_array(b_tree_t *node, tree_item_t *element) {
    if (node->records_count == 0) {
        node->records[node->records_count] = *element;
    } else {
        int i = node->records_count - 1;
        while(element->key < node->records[i].key && i>=0) {
            node->records[i+1] = node->records[i];
            i--;
        }
        node->records[i+1] = *element;
    }
    node->records_count += 1;
}

void print_node_elements(b_tree_t *node) {
    printf("| ");
    for(int i = 0; i < node->records_count; ++i) {
        printf("%d | ", node->records[i].key);
    }
    printf("\n");
}

void insert_promoted_on_parent(b_tree_t *node, promoted_key_t *promoted) {
    printf("current parent -> ");
    print_node_elements(node);

    tree_item_t *element = malloc(sizeof(tree_item_t));
    element->key = promoted->key;
    element->RRN = promoted->RRN;

    int i = 0;
    int leaf_overflowed_index = -1;
    
    while (i < node->child_count && leaf_overflowed_index == -1) { // find the overflowed index
        if (node->childs[i].records_count == BTREE_ORDEM) leaf_overflowed_index = i;
        ++i;
    }

    i = node->child_count;
    while (i >= leaf_overflowed_index) { // bota todos para a direita até chegar no index do overflowed
        node->childs[i+1] = node->childs[i];
        --i;
    }

    // mergear os itens que vieram do valor promovido
    promoted->childs[0].node_parent = node;
    promoted->childs[1].node_parent = node;
    node->childs[leaf_overflowed_index] = promoted->childs[0];
    node->childs[leaf_overflowed_index + 1] = promoted->childs[1];
    node->child_count += 1;

    insert_tree(node, element, 1);
}

void print_everything(b_tree_t *node) {
    print_node_elements(node);
    for (int i =0; i < node->child_count; ++i) {
        printf("leaf %d -> ", i);
        print_everything(&node->childs[i]);
    }
}

int find_leaf_index (b_tree_t *node, tree_item_t *key) {
    int i = 0;
    int index = -1;
    while (i < node->records_count && index == -1) {
        if (node->records[i].key > key->key) {
            index = i;
        }
        ++i;
    }
    if (index == -1) {
        index = node->records_count; // insert on the last position
    }

    return index;
}

b_tree_t* insert_tree(b_tree_t *node, tree_item_t *element, int force_insert) {
    if (node->child_count == 0 || force_insert) {
        insert_sorted_array(node, element);

        if (node->records_count == BTREE_ORDEM) {
            printf("overflow in node \n");
            print_node_elements(node);

            b_tree_t *first_leaf, *second_leaf;
            first_leaf = create_node();
            second_leaf = create_node();
            for (int i = 0; i < (BTREE_ORDEM / 2); ++i) {
                insert_sorted_array(first_leaf, &node->records[i]);
            }

            for (int i = (BTREE_ORDEM / 2) + 1; i < node->records_count; ++i) {
                insert_sorted_array(second_leaf, &node->records[i]);
            }
            printf("two sub arrays created\n");
            print_node_elements(first_leaf);
            print_node_elements(second_leaf);

            promoted_key_t *promoted;
            promoted = malloc(sizeof(promoted_key_t));
            promoted->childs = malloc(BTREE_ORDEM * sizeof *promoted->childs);
            promoted->childs[0] = *first_leaf;
            promoted->childs[1] = *second_leaf;
            promoted->key = node->records[(BTREE_ORDEM / 2)].key;
            promoted->RRN = node->records[(BTREE_ORDEM / 2)].RRN;

            if (node->child_count > 0) {
                printf("splitted node had childs");
                for (int i = 0; i < (node->child_count / 2); ++i) {
                    if (node->childs[i].records[0].key < promoted->key) {
                        node->childs[i].node_parent = first_leaf;
                        first_leaf->childs[first_leaf->child_count] = node->childs[i];
                        first_leaf->child_count += 1;
                    } else {
                        node->childs[i].node_parent = second_leaf;
                        second_leaf->childs[second_leaf->child_count] = node->childs[i];
                        second_leaf->child_count += 1;
                    }
                }

                for (int i = (node->child_count / 2); i < node->child_count; ++i) {
                    if (node->childs[i].records[0].key < promoted->key) {
                        node->childs[i].node_parent = first_leaf;
                        first_leaf->childs[first_leaf->child_count] = node->childs[i];
                        first_leaf->child_count += 1;
                    } else {
                        node->childs[i].node_parent = second_leaf;
                        second_leaf->childs[second_leaf->child_count] = node->childs[i];
                        second_leaf->child_count += 1;
                    }
                }
            }

            if (node->node_parent == NULL) { // should be root
                first_leaf->node_parent = node;
                second_leaf->node_parent = node;
                node->childs[0] = *first_leaf;
                node->childs[1] = *second_leaf;
                node->records[0].key = promoted->key;
                node->records[0].RRN = promoted->RRN;
                node->is_leaf = 0;
                node->is_root = 1;
                node->records_count = 1;
                node->child_count = 2;
            } else {
                // insert promoted item on parent node
                printf("will insert promoted key into parent \n");
                insert_promoted_on_parent(node->node_parent, promoted);
                return node;
            }

            return NULL;
        }
    } else {
        int leaf_to_insert = find_leaf_index(node, element);
        printf("Will insert on leaf %d\n", leaf_to_insert);
        b_tree_t *after_insert = insert_tree(&(node->childs[leaf_to_insert]), element, 0);
        if (after_insert != NULL) {
            // updates?
            node->childs[leaf_to_insert] = *after_insert;
            node = after_insert->node_parent;
        }
        return node;
    }

    return NULL;
}